Break-out LitElement tutorial
=============================

We are going to develop a very simple version of the break-out clasic game using LitElement.
Do you rembember?

```
 === === === === === === === ===
 === === === === === === === ===
 === === ===     === === === ===
 === === ===         === === ===
               o 




            <--->
________________________________
```

...but we are going to develop an Arkanoid clone, hehe, something like this:

![Arkanoik](./screen.png "Arkanoid clone")

- - - 

Steps
=====
- Step  1   - [Get started](https://bitbucket.org/litelement/break-out-tutorial/src/1ebf5b76523321c1551194f4b421b83c795f1443/?at=feature%2Fstep-01)
- Step  2   - [The game area](https://bitbucket.org/litelement/break-out-tutorial/src/132e08bb3b27b836e8f3637bbf6de33cb333794c/?at=feature%2Fstep-02)
- Step  3.1 - [The brick component](https://bitbucket.org/litelement/break-out-tutorial/src/00e3bf375db3f22f86fd0b726eebfcd1874f50a3/?at=feature%2Fstep-03-1)
- Step  3.2 - [Attribute <=> Property conversion](https://bitbucket.org/litelement/break-out-tutorial/src/1a1d856ff216a7462111f8724c9d2828dd3b7307/?at=feature%2Fstep-03-2)
- Step  3.3 - [Lit-html and dot notation](https://bitbucket.org/litelement/break-out-tutorial/src/5b39179fd20e91f3258915227ccf47133eda7770/?at=feature%2Fstep-03-3)
- Step  4.1 - [The padel](https://bitbucket.org/litelement/break-out-tutorial/src/9d1b82b40c5e7b949bde28e82a4159b5f6d38d52/?at=feature%2Fstep-04-1)
- Step  4.2 - [Component inheritance](https://bitbucket.org/litelement/break-out-tutorial/src/b5f26acbe47ca7da9cd4398f5e2ac0a7b4b4d51c/?at=feature%2Fstep-04-2)
- Step  5.1 - [The ball](https://bitbucket.org/litelement/break-out-tutorial/src/175deb90d5fcf0ac21c7030f6a8b4b8a85f2c914/?at=feature%2Fstep-05-1)
- Step  5.2 - [The bouncing ball](https://bitbucket.org/litelement/break-out-tutorial/src/0ff276408da0bf65de4810332c9b9ec4c4e86133/?at=feature%2Fstep-05-2)
- Step  6-1 - [Introducing game loop](https://bitbucket.org/litelement/break-out-tutorial/src/ad5933cd2bea04566a077eae75533a5f48a42dbe/?at=feature%2Fstep-06-1)
- Step  6-2 - [Collisions](https://bitbucket.org/litelement/break-out-tutorial/src/f6a663d6cf52348c61738e39b08a01c305078705/?at=feature%2Fstep-06-2)
- Step  7   - [Slots](https://bitbucket.org/litelement/break-out-tutorial/src/1d4fdece06d48128a2595108a9cc5228017e958e/?at=feature%2Fstep-07)
- Step  8   - [Controlling render](https://bitbucket.org/litelement/break-out-tutorial/src/9c6194d175f273e07260940a2da304d1638a97be/?at=feature%2Fstep-08)
- Step  9   - [Scoreboard](https://bitbucket.org/litelement/break-out-tutorial/src/faf15571e5468d7aef4846fd88a16d521849d413/?at=feature%2Fstep-09)
- Step 10   - [Level, lives and endgame](https://bitbucket.org/litelement/break-out-tutorial/src/959591c6796272b29bf8f96265a7820ec30ee221/?at=feature%2Fstep-10)
- Step 11   - [More bricks, more color, more fun](https://bitbucket.org/litelement/break-out-tutorial/src/108e0dc836b7b670641ab04a78690ff3216ac112/?at=feature%2Fstep-11)
- Step 12   - in progress...

Useful links
=============
lit-element: https://lit-element.polymer-project.org/guide

lit-html: https://lit-html.polymer-project.org/guide

open-wc: https://open-wc.org/index.html

Visual Studio code: https://code.visualstudio.com/
> Recomended extensions: **lit-html** and **lit-plugin**
